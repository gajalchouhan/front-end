import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { map, take } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public baseUrl = 'http://13.232.211.143/globalLogin';
  public baseUrl2 = 'http://13.232.211.143/auth';

  constructor(private http : HttpClient) { }

  public signUpTokenPass(req):any{
    return this.http.post(this.baseUrl2 + '/login' ,req).pipe(map(data => data));
  }

  public signUpOtpPass(req):any{
      return this.http.post(this.baseUrl + '/otpValidator', req).pipe(map(data => data))
  }

  public userSignUp(req): any {
    return this.http.post(this.baseUrl + '/signup', req).pipe(map(data => data));
  }

  public userLogin(req):any{
    return this.http.post(this.baseUrl + '/login', req).pipe(map(data => data));
  }
  
}
