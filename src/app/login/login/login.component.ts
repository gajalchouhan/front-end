import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public signUpEmail : string;
  public signUpPassword :  string;
  public signUpConfirmPassword : string;
  public signUpAlert:boolean = false;
  public signUpResponse:any;
  public loginEmail : string;
  public loginPassword : string;
  constructor( private loginServ : LoginService) { }
  
  public login(){
    let req = {
      "email" : this.loginEmail,
      "password" : this.loginPassword
    }

    this.loginServ.userLogin(req).subscribe(
      res => {
        let data = res.json();
        console.log(data);
      }
    )
  }

  public signUp(){
    if(this.signUpPassword === this.signUpConfirmPassword){
      let req = {
        "email" : this.signUpEmail,
        "password" : this.signUpPassword
      }
      this.loginServ.userSignUp(req).subscribe(
        res => {
         
          console.log(res.status);
          if(res.status === 1){
            let signupOtp = {
              "email" : this.signUpEmail,
              "otp" : 123456
            }
           
            this.loginServ.signUpOtpPass(signupOtp).subscribe(res => {
                if(res.status === 1){
                  let tokenPass = {
                    "token" : res.token
                  }
                  this.loginServ.signUpTokenPass(tokenPass).subscribe(res => {
                    console.log(res.msg);
                  })
                }   
              }
            ) 
          }
        }
      )
    }else{
      this.signUpAlert = true;
    }
  }
  ngOnInit() {
    
  }

}
