import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project';
  public featurePage:boolean = false;

  public loginFeaturePage(){
    this.featurePage = true;

  }

  public homePage(){
    this.featurePage = false;
  }
}
